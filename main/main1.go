package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type taskk struct {
	ID        string `json:"id"`
	Workplace string `json:"workplace"`
	Task      string `json:"description"`
}

var taskss = []taskk{
	{ID: "1", Workplace: "job", Task: "get work done"},
	{ID: "2", Workplace: "home", Task: "cleaning"},
	{ID: "3", Workplace: "anonymous", Task: "shoppping"},
}

func main() {
	router := gin.Default()
	router.GET("/task", getTasks)
	router.GET("/task/:id", getTaskById)
	router.POST("/task", postTasks)
	router.DELETE("/task/:id", deleteTask)
	router.PUT("/task/:id", updateTask)

	// postgres db connection
	connStr := "user=postgres dbname=taskManager password=admin host=localhost sslmode=disable port=5432"
	db, err := sql.Open("postgres", connStr)
  
	if err != nil {
	  panic(err)
	}
	defer db.Close()
  
	err = db.Ping()
	if err != nil {
	  panic(err)
	}
	DB = db
  
	fmt.Printf("\nSuccessfully connected to database!\n")
  
	router.Run("localhost:8080")
  }
  

	

// router function

func getTasks(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, taskss)
}
func postTasks(c *gin.Context) {
	var newtodo taskk
	if err := c.BindJSON(&newtodo); err != nil {
		return
	}
	taskss = append(taskss, newtodo)
	c.IndentedJSON(http.StatusCreated, newtodo)
}

func getTaskById(c *gin.Context) {
	id := c.Param("id")

	for _, a := range taskss {
		if a.ID == id {
			c.IndentedJSON(http.StatusOK, a)
			return
		}
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "task not found"})
}
func deleteTask(c *gin.Context) {
	id := c.Param("id")

	for i, v := range taskss {
		if v.ID == id {
			taskss = append(taskss[:i], taskss[i+1:]...)
			break
		}
	}

	c.IndentedJSON(http.StatusOK, taskss)
}
func updateTask(c *gin.Context) {
	id := c.Param("id")
	var update taskk

	if err := c.BindJSON(&update); err != nil {
		return
	}

	for i, v := range taskss {
		if v.ID == id {
			taskss = append(taskss[:i], taskss[i+1:]...)
			update.ID = id
			taskss = append(taskss, update)
			break
		}
	}
	c.IndentedJSON(http.StatusOK, taskss)

}
/*package jwtAuth
​
import (
	"net/http"
	"time"
​
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)
​
type MyCustomClaims struct {
	Email string `json:"email"`
	Pass  string `json:"password"`
	jwt.RegisteredClaims
}
type Info struct {
	Email string `json:"email"`
	Pass  string `json:"password"`
}
​
var Infos = []Info{}
​
func Auth(email string, pass string) string {
​
	mySigningKey := []byte("thisismysecret")
​
	// create the claims
	claims := &MyCustomClaims{
		email,
		pass,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			Issuer:    "Ishan",
			Subject:   "Jwt token",
		},
	}
​
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, _ := token.SignedString(mySigningKey)
	return ss
}
​
// singup router
func SignUp(c *gin.Context) {
	var info Info
	if err := c.BindJSON(&info); err != nil {
		return
	}
	Infos = append(Infos, info)
	token := Auth(info.Email, info.Pass)
	c.IndentedJSON(http.StatusCreated, gin.H{
		"token": token,
	})
}