package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type taskk struct {
	ID        string `json:"id"`
	Workplace string `json:"workplace"`
	Task      string `json:"description"`
}

var taskss = []taskk{
	{ID: "1", Workplace: "job", Task: "get work done"},
	{ID: "2", Workplace: "home", Task: "cleaning"},
	{ID: "3", Workplace: "anonymous", Task: "shoppping"},
}

func main() {
	router := gin.Default()
	router.GET("/task", getTasks)
	router.GET("/task/:id", getTaskById)
	router.POST("/task", postTasks)
	router.DELETE("/task/:id", deleteTask)
	router.PUT("/task/:id", updateTask)

	router.Run("localhost:8080")
}

// router function

func getTasks(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, taskss)
}
func postTasks(c *gin.Context) {
	var newtodo taskk
	if err := c.BindJSON(&newtodo); err != nil {
		return
	}
	taskss = append(taskss, newtodo)
	c.IndentedJSON(http.StatusCreated, newtodo)
}

func getTaskById(c *gin.Context) {
	id := c.Param("id")

	for _, a := range taskss {
		if a.ID == id {
			c.IndentedJSON(http.StatusOK, a)
			return
		}
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "task not found"})
}
func deleteTask(c *gin.Context) {
	id := c.Param("id")

	for i, v := range taskss {
		if v.ID == id {
			taskss = append(taskss[:i], taskss[i+1:]...)
			break
		}
	}

	c.IndentedJSON(http.StatusOK, taskss)
}
func updateTask(c *gin.Context) {
	id := c.Param("id")
	var update taskk

	if err := c.BindJSON(&update); err != nil {
		return
	}

	for i, v := range taskss {
		if v.ID == id {
			taskss = append(taskss[:i], taskss[i+1:]...)
			update.ID = id
			taskss = append(taskss, update)
			break
		}
	}
	c.IndentedJSON(http.StatusOK, taskss)

}
