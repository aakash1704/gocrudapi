package routers

import (
	"net/http"

	"Golang/jwtAuth"
	"Golang/models"

	"github.com/gin-gonic/gin"
)

type taskk struct {
	ID        string `json:"id"`
	Workplace string `json:"workplace"`
	Task      string `json:"task"`
}

var DB = models.Init()

func RegisterRoutes() *gin.Engine {
	router := gin.Default()

	//routes
	router.GET("/task", getTasks)
	router.GET("/task/:id", getTaskById)
	router.POST("/task", postTasks)
	router.POST("/signup", jwtAuth.SignUp)
	router.DELETE("/task/:id", deleteTask)
	router.PUT("/task/:id", updateTask)

	return router
}

//get all tasks
func getTasks(c *gin.Context) {
	var allTask []taskk
	out, err := DB.Query(`SELECT * from db_api`)
	if err != nil {
		panic(err)
	}
	for out.Next() {
		var id string
		var workplace string
		var task string
		err = out.Scan(&id, &workplace, &task)
		if err != nil {
			panic(err)
		}
		allTask = append(allTask, taskk{ID: id, Workplace: workplace, Task: task})

	}
	c.IndentedJSON(http.StatusOK, allTask)
}

func postTasks(c *gin.Context) {
	var newtodo taskk

	if err := c.BindJSON(&newtodo); err != nil {
		return
	}

	/* todos = append(todos, newtodo) */
	sqlStmnt := `INSERT INTO db_api (id,workplace,task) VALUES ($1,$2,$3)`
	_, err := DB.Exec(sqlStmnt, newtodo.ID, newtodo.Workplace, newtodo.Task)
	if err != nil {
		panic(err)
	}
	c.IndentedJSON(http.StatusCreated, newtodo)
}

func getTaskById(c *gin.Context) {
	id := c.Param("id")
	var allTask []taskk
	sqlStmnt := `SELECT * from db_api WHERE ID IN ($1)`
	out, err := DB.Query(sqlStmnt, id)
	if err != nil {
		panic(err)
	}
	for out.Next() {
		var id string
		var workplace string
		var task string
		err = out.Scan(&id, &workplace, &task)
		if err != nil {
			panic(err)
		}
		allTask = append(allTask, taskk{ID: id, Workplace: workplace, Task: task})

	}
	c.IndentedJSON(http.StatusOK, allTask)
	/*for _, a := range todos {
	  if a.ID == id {
	    c.IndentedJSON(http.StatusOK, a)
	    return
	  }
	}*/
	//c.IndentedJSON(http.StatusNotFound, gin.H{"message": "task not found"})
}

func updateTask(c *gin.Context) {
	id := c.Param("id")
	var update taskk

	if err := c.BindJSON(&update); err != nil {
		return
	}

	sqlStmnt := `UPDATE db_api SET workplace=$1, task=$2 WHERE id=$3`
	_, err := DB.Exec(sqlStmnt, update.Workplace, update.Task, id)
	if err != nil {
		panic(err)
	}
	c.IndentedJSON(http.StatusOK, gin.H{"message": "updated successfully"})
}

func deleteTask(c *gin.Context) {
	id := c.Param("id")
	sqlStatement := `DELETE FROM db_api WHERE id = $1;`
	_, err := DB.Exec(sqlStatement, id)
	if err != nil {
		panic(err)
	}
	c.IndentedJSON(http.StatusOK, gin.H{"message": "deleted"})
}
